<?php
$DEBUG = true;							// Priprava podrobnejših opisov napak (med testiranjem)
 
include("orodja.php"); 					// Vključitev 'orodij'
 
$zbirka = dbConnect();					// Pridobitev povezave s podatkovno zbirko
 
header('Content-Type: application/json');	// Nastavimo MIME tip vsebine odgovora
 
switch($_SERVER["REQUEST_METHOD"])		// Glede na HTTP metodo v zahtevi izberemo ustrezno dejanje nad virom
{
	case 'GET':
		if(!empty($_GET["vzdevek"]) && !empty($_GET["vloga"]))
		//tako imenovana "ZASTAVICA"! POTREBNO PREVERITI OBE VREDNOSTI
		//DA SE IZVEDE 1.if, IN lahko zato pridobimo vlogo
		//alternativno BREZ DODATNEGA HTACESSA ZA TIP_PRIJAVE!!!!
		{
			tip_prijave($_GET["vzdevek"]);
		}
		else if(!empty($_GET["vzdevek"]))
		{
			pridobi_osebo($_GET["vzdevek"]);		// Če odjemalec posreduje vzdevek, mu vrnemo podatke izbrane osebe
		}
		else
		{
			pridobi_vse_osebe();					// Če odjemalec ne posreduje vzdevka, mu vrnemo podatke vseh oseb
		}
		break;
 
	// ******* Dopolnite še z dodajanjem, posodabljanjem in brisanjem oseb
 
	case 'POST':
		dodaj_osebo();
		break;
 
	case 'PUT':
		if(!empty($_GET["vzdevek"]))
		{
			posodobi_osebo($_GET["vzdevek"]);
		}
		else
		{
			http_response_code(404);	// Not found
		}
 
		break;

	case 'DELETE':
	
		if(!empty($_GET["vzdevek"]))
		{
			izbrisi_osebo($_GET["vzdevek"]);
		}
		else
		{
			http_response_code(404);	// Not found
		}
		break;
 
	default:
		http_response_code(405);		//Če naredimo zahtevo s katero koli drugo metodo je to 'Method Not Allowed'
		break;
}
 
mysqli_close($zbirka);					// Sprostimo povezavo z zbirko
//status kode 200, konec programa.
 
// ----------- konec skripte, sledijo funkcije -----------
 
function pridobi_vse_osebe()
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba="SELECT vzdevek, ime, priimek, email, vloga FROM oseba";	
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);		//OK
	echo json_encode($odgovor);
}
 
function pridobi_osebo($vzdevek)
{
	global $zbirka;
	$vzdevek=mysqli_escape_string($zbirka, $vzdevek);
 
	$poizvedba="SELECT vzdevek, ime, priimek, email, vloga FROM oseba WHERE vzdevek='$vzdevek'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	if(mysqli_num_rows($rezultat)>0)	//oseba obstaja
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);		//OK
		echo json_encode($odgovor);
	}
	else							// oseba ne obstaja
	{
		http_response_code(404);		//Not found
	}
}
// *********** Dopolnite še z ostalimi funkcijami
 
function dodaj_osebo()
{
	global $zbirka, $DEBUG;
	
	$podatki = json_decode(file_get_contents("php://input"),true);//iz bodija potegne ven json, če npr. dodajamo, posodabljamo
	
	if(isset($podatki["vzdevek"], $podatki["geslo"], $podatki["ime"], $podatki["priimek"], $podatki["email"], $podatki["vloga"]))
	{	
		$vzdevek = mysqli_escape_string($zbirka, $podatki["vzdevek"]);
		$geslo = hash("md5", mysqli_escape_string($zbirka, $podatki["vzdevek"].$podatki["geslo"]));
		#$geslo = password_hash(mysqli_escape_string($zbirka, $podatki["geslo"]), PASSWORD_DEFAULT);
		//stari hash- "pi kript" doda vedno točno določeno začetno sekvenco hash-u kar je dobra stvar,
		//slaba pa je da je zastarel in je mnogo krajši od md5
		$ime = mysqli_escape_string($zbirka, $podatki["ime"]);
		$priimek = mysqli_escape_string($zbirka, $podatki["priimek"]);
		$email = mysqli_escape_string($zbirka, $podatki["email"]);
		$vloga = mysqli_escape_string($zbirka, $podatki["vloga"]);
 
		// ali oseba ze obstaja?
		if(!oseba_obstaja($vzdevek))
		{
			// izvrsimo poizvedbo
			$poizvedba="INSERT INTO oseba (vzdevek, geslo, ime, priimek, email, vloga) VALUES ('$vzdevek', '$geslo', '$ime', '$priimek', '$email', '$vloga')";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(201);	// Created
				$odgovor = URL_vira($vzdevek);
				echo json_encode($odgovor);
			}
			else
			{
				http_response_code(500);	// Internal server error (ni vedno strežnik kriv!)
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
		}
		else
		{
			http_response_code(409);	// Conflict!
			pripravi_odgovor_napaka("Oseba že obstaja!");
		}
	}
	else
	{
		http_response_code(400);	// Bad request
	}
}
 
function posodobi_osebo($vzdevek)
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
 
	if(oseba_obstaja($vzdevek))
	{
		if(isset($podatki["ime"], $podatki["priimek"], $podatki["email"], $podatki["vloga"]))
		{	
			$ime = mysqli_escape_string($zbirka, $podatki["ime"]);
			$priimek = mysqli_escape_string($zbirka, $podatki["priimek"]);
			$email = mysqli_escape_string($zbirka, $podatki["email"]);
			$vloga = mysqli_escape_string($zbirka, $podatki["vloga"]);
 
			// izvrsimo poizvedbo
			
			$poizvedba="UPDATE oseba SET ime='$ime', priimek='$priimek', email='$email', vloga='$vloga' WHERE vzdevek='$vzdevek'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);	// OK with no content
			}
			else
			{
				http_response_code(500);	// Internal server error (ni vedno strežnik kriv!)
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
		}
		else
		{
			http_response_code(400);	// Bad request
		}
	}
	else
	{
		http_response_code(404);	// Not found
	}
}

function izbrisi_osebo($vzdevek) 
{
	global $zbirka, $DEBUG;
	
			// izvrsimo poizvedbo
			$poizvedba="DELETE from oseba WHERE vzdevek='$vzdevek'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);	// OK with no content
			}
			else
			{
				http_response_code(500);	// Internal server error (ni vedno strežnik kriv!)
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}

function tip_prijave($vzdevek)
{							 
	global $zbirka, $DEBUG;
 
			$poizvedba="SELECT vloga FROM oseba WHERE vzdevek='$vzdevek'";
			
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			if(mysqli_num_rows($rezultat)>0)	//obstaja ?
			{
				$odgovor=mysqli_fetch_assoc($rezultat);
	 
				http_response_code(200);		//OK
				echo json_encode($odgovor);
			}
			else
			{
				http_response_code(500);	// Internal server error (ni vedno strežnik kriv!)
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}
?>