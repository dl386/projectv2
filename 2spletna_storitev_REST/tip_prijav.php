<?php
$DEBUG = true;							// Priprava podrobnejših opisov napak (med testiranjem)
 
include("orodja.php"); 					// Vključitev 'orodij'
 
$zbirka = dbConnect();					// Pridobitev povezave s podatkovno zbirko
 
header('Content-Type: application/json');	// Nastavimo MIME tip vsebine odgovora
 
switch($_SERVER["REQUEST_METHOD"])		// Glede na HTTP metodo v zahtevi izberemo ustrezno dejanje nad virom
{
	case 'GET':
		if(!empty($_GET["vloga"]))
		{
			pridobi_tip_prijave($_GET["vloga"]);					// Če odjemalec ne posreduje vzdevka, mu vrnemo podatke vseh oseb
		}
		else
		{
			pridobi_vse_tipe_prijav();					// Če odjemalec ne posreduje vzdevka, mu vrnemo podatke vseh oseb
		}
		break;
 
	// ******* Dopolnite še z dodajanjem, posodabljanjem in brisanjem oseb
 
	default:
		http_response_code(405);		//Če naredimo zahtevo s katero koli drugo metodo je to 'Method Not Allowed'
		break;
}
 
mysqli_close($zbirka);					// Sprostimo povezavo z zbirko
//status kode 200, konec programa.
 
// ----------- konec skripte, sledijo funkcije -----------

function pridobi_tip_prijave($vloga)
{
	global $zbirka;
	$vloga=mysqli_escape_string($zbirka, $vloga);
 
	$poizvedba="SELECT vloga, opis FROM tip_prijave WHERE vloga='$vloga'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	if(mysqli_num_rows($rezultat)>0)	//vloga obstaja
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);		//OK
		echo json_encode($odgovor);
	}
	else							// vloga ne obstaja
	{
		http_response_code(404);		//Not found
	}
}
function pridobi_vse_tipe_prijav()
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba="SELECT * FROM tip_prijave";	
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);		//OK
	echo json_encode($odgovor);
}