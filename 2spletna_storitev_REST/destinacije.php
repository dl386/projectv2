<?php
$DEBUG = true;							// Priprava podrobnejših opisov napak (med testiranjem)
 
include("orodja.php"); 					// Vključitev 'orodij'
 
$zbirka = dbConnect();					// Pridobitev povezave s podatkovno zbirko
 
header('Content-Type: application/json');	// Nastavimo MIME tip vsebine odgovora
 
switch($_SERVER["REQUEST_METHOD"])		// Glede na HTTP metodo v zahtevi izberemo ustrezno dejanje nad virom
{
	case 'GET':
		if(!empty($_GET["IDdestinacije"]))
		{
			pridobi_destinacijo($_GET["IDdestinacije"]);									
		}
		else
		{
			pridobi_vse_destinacije();
		}
		break;
 
	default:
		http_response_code(405);		//Če naredimo zahtevo s katero koli drugo metodo je to 'Method Not Allowed'
		break;
} 
 
mysqli_close($zbirka);					// Sprostimo povezavo z zbirko

//konec skripte sledijo funkcije
function pridobi_destinacijo($IDdestinacije)
{
	global $zbirka;
	
	$IDdestinacije=mysqli_escape_string($zbirka, $IDdestinacije);
 
	$poizvedba="Select ime_destinacije FROM destinacija";
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	if(mysqli_num_rows($rezultat)>0)	//destinacija obstaja
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);		//OK
		echo json_encode($odgovor);
	}
	else							// destinacija ne obstaja
	{
		http_response_code(404);		//Not found
	}
}
 
function pridobi_vse_destinacije() 
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba= "SELECT * FROM destinacija";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);		//OK
	echo json_encode($odgovor);
}