<?php
$DEBUG = true;							// Priprava podrobnejših opisov napak (med testiranjem)
 
include("orodja.php"); 					// Vključitev 'orodij'
 
$zbirka = dbConnect();					// Pridobitev povezave s podatkovno zbirko
 
header('Content-Type: application/json');	// Nastavimo MIME tip vsebine odgovora
 
switch($_SERVER["REQUEST_METHOD"])		// Glede na HTTP metodo v zahtevi izberemo ustrezno dejanje nad virom
{
	case 'GET':
		if(!empty($_GET["vzdevek"]))
		{
			pridobi_rezervacijo_osebe($_GET["vzdevek"]); //če uporabnik vnese vzdevek mu vrnemo
												//točen opis rezervacije (IDpotovanja->vezan na celotno potovanje) 
												//določene osebe
		}
		//18.9. za prikaz po IDrezervacijah, uporabili GET parameter = nov "access rule"
		else if(!empty($_GET["IDrezervacije"]))
		{
			pridobi_rezervacijo_osebe_ID($_GET["IDrezervacije"]);
		}
		//18.9.
		else if(!empty($_GET["vzdevekVse"]))//vse ID rezervacije po vzdevku
		{
			pridobi_vse_rezervacije_vzdevek($_GET["vzdevekVse"]);
		}
		else
		{
			pridobi_vse_rezervacije();
		}
		break;
 
	case 'POST':				
	dodaj_rezervacijo();
	break;
	
	case 'PUT':
		if(!empty($_GET["IDrezervacije"]))
		{
			posodobi_rezervacijo($_GET["IDrezervacije"]);
		}
		else
		{
			http_response_code(404);	// Not found
		}
		break;

	case 'DELETE':
		/*agencije in tudi admin!!!
		VPRAŠAJ! ZA kasneje, recimo tudi brisanje pretečenih terminov oziroma tudi potovanj
		pazi pri omejitvah, kdo lahko briše in kdo ne (restricted samo za določen tip prijave?)
		*/
		if(!empty($_GET["IDrezervacije"]))
		{
		izbrisi_rezervacijo($_GET["IDrezervacije"]);
		}
		else
		{
			http_response_code(404);	// Not found
		}
		break;
		
	default:
		http_response_code(405);		//Če naredimo zahtevo s katero koli drugo metodo je to 'Method Not Allowed'
		break;
}
 
 
mysqli_close($zbirka);					// Sprostimo povezavo z zbirko
 
function pridobi_vse_rezervacije()
{
	global $zbirka;
	$odgovor=array();
 
	//$poizvedba="SELECT rezervacija.vzdevek, rezervacija.IDrezervacije, potovanje.IDdestinacije, destinacija.ime_destinacije, rezervacija.casovni_zig, potovanje.datum, potovanje.trajanje, potovanje.agencija, potovanje.cena, potovanje.opis_aranzmaja FROM potovanje JOIN destinacija ON potovanje.IDdestinacije=destinacija.IDdestinacije JOIN rezervacija";
	$poizvedba= "SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, rezervacija.IDpotovanja, rezervacija.casovni_zig, potovanje.agencija FROM rezervacija JOIN potovanje ON potovanje.IDpotovanja = rezervacija.IDpotovanja";
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
	
	http_response_code(200);		//OK
	echo json_encode($odgovor);
}

function pridobi_rezervacijo_osebe($vzdevek)
{
	global $zbirka;
	$odgovor=array();
	$vzdevek=mysqli_escape_string($zbirka, $vzdevek);
 
	$poizvedba="SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, potovanje.IDpotovanja, destinacija.ime_destinacije, rezervacija.casovni_zig, potovanje.agencija FROM potovanje JOIN destinacija ON potovanje.IDdestinacije=destinacija.IDdestinacije JOIN rezervacija ON potovanje.IDpotovanja=rezervacija.IDpotovanja WHERE vzdevek='$vzdevek'"; //WHERE vzdevek='mojca'//$poizvedba=inner join....
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
		http_response_code(200);		//OK
		echo json_encode($odgovor);
}

function pridobi_vse_rezervacije_vzdevek($vzdevek)
{
	global $zbirka;
	$odgovor=array();
 
	//$poizvedba="SELECT rezervacija.vzdevek, rezervacija.IDrezervacije, potovanje.IDdestinacije, destinacija.ime_destinacije, rezervacija.casovni_zig, potovanje.datum, potovanje.trajanje, potovanje.agencija, potovanje.cena, potovanje.opis_aranzmaja FROM potovanje JOIN destinacija ON potovanje.IDdestinacije=destinacija.IDdestinacije JOIN rezervacija";
	$poizvedba= "SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, rezervacija.IDpotovanja, rezervacija.casovni_zig, potovanje.agencija FROM rezervacija JOIN potovanje ON potovanje.IDpotovanja = rezervacija.IDpotovanja WHERE vzdevek='$vzdevek'";
	//18.9. POPRAVEK query-ja pri pogoju prikazujemo pri 
	//get parametru "vzdevekVse" samo po vzdevku vseh rezervacij
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);		//OK
	echo json_encode($odgovor);
}

function pridobi_rezervacijo_osebe_ID($IDrezervacije)
{
	global $zbirka;
	//$odgovor=array(); PODOBNA SITUACIJA KOT PRI AGENCIJI za potovanja po ID-ju
	//potrebujemo objekt enega vnosa in ne več array-ev!
	$IDrezervacije=mysqli_escape_string($zbirka, $IDrezervacije);
 
	$poizvedba="SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, potovanje.IDpotovanja, destinacija.ime_destinacije, rezervacija.casovni_zig, potovanje.agencija FROM potovanje JOIN destinacija ON potovanje.IDdestinacije=destinacija.IDdestinacije JOIN rezervacija ON potovanje.IDpotovanja=rezervacija.IDpotovanja WHERE IDrezervacije='$IDrezervacije'"; //WHERE IDrezervacije='31'//$poizvedba=inner join....
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	if(mysqli_num_rows($rezultat)>0)	//rezervacija obstaja
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);		//OK
		echo json_encode($odgovor);
	}
	else							// rezervacija ne obstaja
	{
		http_response_code(404);		//Not found
	}
}

function dodaj_rezervacijo() 
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
	
	$vzdevek = mysqli_escape_string($zbirka, $podatki["vzdevek"]);
	$IDpotovanja = mysqli_escape_string($zbirka, $podatki["IDpotovanja"]);
 
	if(isset($podatki["vzdevek"], $podatki["IDpotovanja"]));
	{	
	$poizvedba="SELECT *from rezervacija WHERE vzdevek='$vzdevek' and IDpotovanja='$IDpotovanja'";
		
		// Step 3: Execute the query
		$result = mysqli_query($zbirka, $poizvedba);

		// Step 4: Check if any rows were returned
		if (mysqli_num_rows($result) > 0) {
			echo //"Query return more then one row.";
			"rezervacija osebe že obstaja (se ne more rezervirati na isto IDpotovanje! saj ni klon)";
		} 
		else 
		{
			//echo //"Query did not return any rows.";
			$poizvedba="INSERT INTO rezervacija (vzdevek, IDpotovanja) VALUES ('$vzdevek', '$IDpotovanja')";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(201);	// Created
				$odgovor = URL_vira($vzdevek);
				echo json_encode($odgovor);
			}
		}
	}
}
function posodobi_rezervacijo($IDrezervacije)
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
	
		if(isset($podatki["IDrezervacije"], $podatki["IDpotovanja"]))
		{	
			$IDrezervacije = mysqli_escape_string($zbirka, $podatki["IDrezervacije"]);
			$IDpotovanja = mysqli_escape_string($zbirka, $podatki["IDpotovanja"]);
			
			$poizvedba="UPDATE rezervacija SET IDpotovanja='$IDpotovanja' WHERE IDrezervacije='$IDrezervacije'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);	// OK with no content
			}
			else
			{
				http_response_code(500);	// Internal server error (ni vedno strežnik kriv!)
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
		}
		else
		{
			http_response_code(409);	// Bad request
		}
}
function izbrisi_rezervacijo($IDrezervacije)
{
	global $zbirka, $DEBUG;
	
			// izvrsimo poizvedbo
			$poizvedba="DELETE from rezervacija WHERE IDrezervacije='$IDrezervacije'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);	// OK with no content
			}
			else
			{
				http_response_code(500);	// Internal server error (ni vedno strežnik kriv!)
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}		
?>