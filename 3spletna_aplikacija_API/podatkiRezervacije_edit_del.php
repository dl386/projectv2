<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Potovalni nacrt - Posodabljanje/brisanje rezervacije</title>
		<link rel="stylesheet" type="text/css" href="stil.css" />
	</head>
	<body>
		<div class="center">
			<?php include "Meni3.html"?>
			</div>
			<form onsubmit="podatkiRezervacije_edit_del(); return false;" id="obrazec">
				<div style="background-image: url('uporabnik.png'); height:85% ; width:99%; position:absolute; background-repeat:repeat-y; background-size:100% 100%">
				<div class="center">
				<label class="pisavaUpor" for="IDrezervacijeVsi">Rezervacija:</label>
				<input type="text" id="IDrezervacijeVsi" list="options-list" required />
				<input type="submit" value="Prikaži" />
				<datalist id="options-list">
				</datalist>		
			</form>			
			<div class="pisavaUpor" id="odgovor"></div>
		</div></div>
		<script src="JS/podatkiRezervacije_edit_del.js"></script>
	</body>
</html>