<?php
function dbConnect(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "potovalni_nacrt";

	// Ustvarimo povezavo do podatkovne zbirke
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	mysqli_set_charset($conn,"utf8");

	// Preverimo uspeh povezave
	if (mysqli_connect_errno()) {
		printf("Povezovanje s podatkovnim strežnikom ni uspelo: %s\n", mysqli_connect_error());
		exit();
	} 	
	return $conn;
	mysqli_close($zbirka);
	}
?>