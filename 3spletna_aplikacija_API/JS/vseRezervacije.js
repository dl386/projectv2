function vseRezervacije()
{
	var xmlhttp = new XMLHttpRequest();										// ustvarimo HTTP zahtevo
 
	xmlhttp.onreadystatechange = function()									// določimo odziv v primeru različnih razpletov komunikacije
	{
		if (this.readyState == 4 && this.status == 200)						// zahteva je bila uspešno poslana, prišel je odgovor 201
		{
			try{
				var podatki = JSON.parse(this.response);
				prikaziPodatke(podatki);
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/rezervacije_oseb", true);							// določimo metodo in URL zahteve, izberemo asinhrono zahtevo (true)
	xmlhttp.send();													// priložimo podatke in izvedemo zahtevo
}
 
function prikaziPodatke(odgovorJSON){
   var fragment = document.createDocumentFragment();		// Zaradi učinkovitosti uporabimo fragment.
 
   for (var i = 0; i < odgovorJSON.length; i++) {			// Za vsak objekt v JSONu ...
		var tr = document.createElement("tr");					// ... ustvarimo vrstico v tabeli (tr).
 
      for(var stolpec in odgovorJSON[i]){					// Za vsak stolpec v objektu ...
         var td = document.createElement("td");				// ... ustvarimo celico (td) ...
         td.innerHTML = odgovorJSON[i][stolpec];			// ... in vanjo zapišemo vrednost stolpca.
         tr.appendChild(td);								// Celico dodamo v vrstico tabele.
      }
      fragment.appendChild(tr);		       					// Vrstico tabele dodamo v fragment.
   }
   document.getElementById("tabela").appendChild(fragment);	// Fragment dodamo v obstoječo tabelo.
}  