const formToJSON = elements => [].reduce.call(elements, (data, element) => 
{
	if(element.name!="")
	{
		data[element.name] = element.value;
	}
  return data;
}, {});

function prijava(upime, geslo) {
	
	var params = "upime=" + upime + "&geslo=" + geslo;

	var xhr= new XMLHttpRequest();
	xhr.open("POST", "backend/login.php", true); // vpisemo URL mesta za avtentikacijo v API-ju zaledja
	///PAZI!!!!!!!!!!! 6.8. sprememba!
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	xhr.onreadystatechange = function () {
	   if (xhr.readyState === 4) {
		  if(xhr.status === 200) {
			  console.log(xhr.responseText);
			  var response = JSON.parse(xhr.responseText);
			  
			  window.localStorage.setItem("access_token", response["token"]);//zeton shranimo v lokalno shrambo
			  window.localStorage.setItem("username", response["username"]);
			  oPrijavi();	// posodobimo spletno stran
			}
			else{
				alert("Prijava ni uspela!");
			}
	   }};
	xhr.send(params);
}

function oPrijavi() {
	var zeton = window.localStorage.getItem('access_token');
	if(zeton != null && zeton != ""){
		
		var username = window.localStorage.getItem("username");
				window.location = "Meni3.html"
		}		
	}
function dodajOsebo()
{
	
	const data = formToJSON(document.getElementById("obrazec").elements);	// vsebino obrazca pretvorimo v objekt
	var username = data['vzdevek'];
	
	var geslo = data['geslo'];
	
	var JSONdata = JSON.stringify(data, null, "  ");						// objekt pretvorimo v znakovni niz v formatu JSON
	var xmlhttp = new XMLHttpRequest();										// ustvarimo HTTP zahtevo
	 
	xmlhttp.onreadystatechange = function()									// določimo odziv v primeru različnih razpletov komunikacije
	{
		if (this.readyState == 4 && this.status == 201)						// zahteva je bila uspešno poslana, prišel je odgovor 201
		{
			prijava(username, geslo); //
		}
		if(this.readyState == 4 && this.status != 201)						// zahteva je bila uspešno poslana, prišel je odgovor, ki ni 201
		{//primer neuspesno dodajanje pri omejitev polj ali kaj podobnega
			document.getElementById("odgovor").innerHTML="Dodajanje ni uspelo: "+this.status;
		}
	};
	 
	xmlhttp.open("POST", "http://localhost/PROJEKT/2spletna_storitev_REST/osebe", true); // določimo metodo in URL zahteve, izberemo asinhrono zahtevo (true)
	xmlhttp.send(JSON.stringify(data));													// priložimo podatke in izvedemo zahtevo
}