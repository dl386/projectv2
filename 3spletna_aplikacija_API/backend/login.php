<?php 
$DEBUG = true;							// Priprava podrobnejših opisov napak (med testiranjem)
 
include("../orodja.php"); 					// Vključitev 'orodij'
 
$zbirka = dbConnect();					// Pridobitev povezave s podatkovno zbirko
 
header('Content-Type: application/json'); // Nastavimo MIME tip vsebine odgovora

$headers = apache_request_headers();

			if(isset($_POST["upime"],$_POST["geslo"]))
			{
				if(preveri_osebo($_POST["upime"], $_POST["geslo"]))
				{
					// Naslednja vrstica ustvari zeton na podlagi uporabniskega imena in gesla in je za isto kombinacijo obeh vedno enak.
					// To naredi primer preprost, saj se lahko preverjanje veljavnosti zetona izvede na isti nacin kot tvorjenje, 
					// zato zetona na strezniku ni potrebno shraniti (glejte podatki.php). 
					// Z vidika varnosti pa je boljsi pristop tvorjenje nakljucnega zetona, ki ima omejeno casovno veljavnost.

					$token = hash("md5", $_POST["upime"].$_POST["geslo"]);
					
					echo json_encode(array('token'=>$token, 'username'=>$_POST["upime"]));
				}
				else{
					//v nasprotnem primeru zavrnemo avtentikacijo
					
					http_response_code(401);
				}
			}
			else{
				http_response_code(401);
			}
			mysqli_close($zbirka);
	 
	function preveri_osebo($vzdevek, $geslo)
	{
		global $zbirka, $DEBUG;
		
		$vzdevek=mysqli_escape_string($zbirka, $vzdevek);
		$geslo=mysqli_escape_string($zbirka, $geslo);
		$geslo_hash = hash("md5", mysqli_escape_string($zbirka, $vzdevek.$geslo));
		
		$poizvedba="SELECT vzdevek, ime, priimek, email, vloga FROM oseba WHERE vzdevek='$vzdevek' AND geslo='$geslo_hash'";
		
		$rezultat=mysqli_query($zbirka, $poizvedba);
	 
		if(mysqli_num_rows($rezultat)>0)	//oseba obstaja
		{
			return true;
		}
		else							// oseba ne obstaja
		{
			return false;
		}
	}
?>